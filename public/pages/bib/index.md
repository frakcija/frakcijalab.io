<!--
.. title: Bibliografija / Bibliography
.. author: Centar za dramsku umjetnost / Centre for Drama Art
.. date: 2019-12-02 19:52:05 UTC
.. slug: bib
.. previewimage: /images/Frakcija_01.jpg
.. description: Detaljna bibliografija Frakcije. Frakcija's detailed bibliography.
-->

# FRAKCIJA 1995. - 2015.

*scroll down for English*

***

Pod sljedećom poveznicom možete naći sistematiziranu bibliografiju časopisa za izvedbene umjetnosti Frakcija:

<https://docs.google.com/spreadsheets/d/1T-XfmnMiNEphFkN6b21AtXlyuRYhbxDXNDp0VTH6lRI/edit?usp=sharing>

Ako radije želite skinuti tablicu, kliknite [ovdje](/FRAKCIJA_1995-2015_bibliografija_bibliography.xlsx).

Bibliografija je u obliku tablice, s nekoliko kartica (tabova):

- temati (kronološki, uz godinu, jezik temata, imena urednika temata);
- urednici;
- članci (prema prezimenu autora, uz broj temata i broj stranice, odvojeno za hrvatski i engleski prijevod članka);
- intervjui (prema prezimenu intervjuirane osobe, uz naslov i ime osobe koja je vodila razgovor, odvojeno za hrvatski i engleski prijevod članka);
- info prilozi (prilozi uredništva iz ranijih godina časopisa).

Bibliografiju možete preuzeti, kopirati ili ispisati. Također i dalje dijeliti putem poveznice.

Unutar svake kartice redove podataka možete sortirati prema npr. imenu autora ili broju izdanja (izbornik: Data).

Sve kartice možete pretraživati koristeći Ctrl+F na Windows i Linux operativnim sistemima ili Command+F na Mac računalima.

***

This link below will take you to the systematized bibliography of Frakcija Performing Arts Journal:

<https://docs.google.com/spreadsheets/d/1T-XfmnMiNEphFkN6b21AtXlyuRYhbxDXNDp0VTH6lRI/edit?usp=sharing>

If you rather wish to download the spreadsheet, click [here](/FRAKCIJA_1995-2015_bibliografija_bibliography.xlsx).

The bibliography is in spreadsheet format, with several tabs:

- thematic issues (chronological, with year, language, name of issue editors);
- editors;
- articles (by last name of author, with issue number and page number, separated for Croatian and English translations);
- interviews (by last name of interviewee, with name of interviewer, separated for Croatian and English translations);
- info articles (editors’ contributions in the early years of the journal).

You can download, copy or print the bibliography. Or share it further.

You can sort the rows within each tab by, for example, name of author or number of issue (menu: Data).

You can search for a key word or phrase using Ctrl+F on Windows and Linux operating systems or Command+F on Mac computers.
