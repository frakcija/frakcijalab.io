<!--
.. title: Welcome to FRAKCIJA's Digital Archive!
.. author: Centar za dramsku umjetnost / Centre for Drama Art
.. date: 2019-12-02 19:52:05 UTC
.. previewimage: /images/Frakcija_01.jpg
.. description: The digital archive of the Performing Arts Journal Frakcija, published from 1995 to 2015, includes all of the issues in PDF format and a detailed bibliography.
-->

***

# Frakcija's History

The project of a public digital archive of Frakcija Performing Arts Journal, published 1995 to 2015, allows the professional but also the wider public an insight into two decades of one of the longer lasting publishing projects in Croatia - not just in the independent cultural field, but amongst cultural journals as such.

Frakcija Journal’s publisher, the [Centre for Drama Art](http://arhiva.cdu.hr/news/index.php) in Zagreb, was established in 1995 with the support of the Open Society and in collaboration with the [Academy of Dramatic Art](https://www.adu.unizg.hr/academy-of-dramatic-art/introduction/
) at the University of Zagreb. It initiated an academic publication Teatar & Teorija (T&T), a documentary film production project [Factum](http://factum.com.hr/en) and the Imaginary Academy in Grožnjan, Istria. The, then magazine and later journal of performing arts, Frakcija was established the same year. During its two decades Frakcija transformed into a bilingual (Croatian and English language) internationally relevant journal convening expert collaborators and participating in a multitude of institutional and independent projects.

Established out of a need to articulate a theoretical-critical platform of contemporary movements in performing arts, Frakcija was active in developing the context for fresh and upcoming art, supporting the wider independent art scene in Croatia - a project that, in the 1990s in particular, meant political opposition and defiance of then dominant cultural policy. This progressive and internationally relevant journal is a vessel of institutional memory of many organizations, institutions, artists and artistic groups active in Croatia 1995 - 2015.

Since its inception Frakcija showcased the work of other actors of the scene - Eurokaz Festival, Dance Week Festival, Art Workshop Lazareti in Dubrovnik, Urban Festival, Theater &TD, Theater EXIT, Montažstroj, Trafik, BADco. and so many others. The Center for Drama Art in Zagreb was an active participant in a series of collaborative platforms and networks, including Zagreb - Cultural Kapital of Europe 3000, [Clubture Network](https://www.clubture.org/info/about-us?l=en
) and [Alliance Operation City](https://operacijagrad.net/savez-udruga-operacija-grad/). The journal’s issues and their editorial policy and politics did not just follow but actively supported and co-created movements in the local cultural-political scene, and the journal’s collaborators were key players in the cultural policy field in the two decades of publishing, many remain so (Vjeran Zuppa, Goran Sergej Pristaš, Ivica Buljan, Milan Živković, Emina Višnić, Marin Blažević and others).

In its second decade, as a bilingual publication, Frakcija expanded its readership abroad, engaging Croatian, European and international scholars, theatrologists, artists, experts and members of the wider public. If we consider international reactions, the journal’s own initiatives, collaborations with other publications ([Performance Research](https://www.performance-research.org/), [Maska](http://www.maska.si/index.php?id=161&L=1&id=161), The Drama Review...), centers for performing arts (Chapter Arts Centre Cardiff, Kampnagel Hamburg, Intercult Stockholm, TQW, Tanzkongress), universities and academic institutions (Dartington College of Arts, DasArts Amsterdam, University of Wisconsin-Milwaukee) and professional networks (Performance Studies International, Theorem) Frakcija engaged from year to year through a series of projects - this was truly an outstanding endeavor.

Frakcija is also a recipient of many prestigious design awards in the hands of its designers Igor Masnjak, CavarPayer, Laboratorium and Dejan Dragosavac Ruta: European Regional Design Annual Certificate of Design Excellence in 1998, Annual Design Review Certificate of Excellence in 2003, YoungGuns International Advertising & Design Award in 2002, Icograda Excellence Award in 2003, iF communication design award in 2005, just to name a few. After appearing in many local and international exhibitions of design, Frakcija was exhibited in a solo show at the [Croatian Designers Association](http://dizajn.hr/fragmenti-dizajnerske-povijesti-dokumenti-vol-1/3-drugi-rakurs/3-4-frakcija-1996-2010/) on occasion of it’s 15th year of publishing (curated by: Maroje Mrduljaš).

# About the Digital Archive

The project of the digital archive of Frakcija includes the digitization of all the journal’s issues, a permanent and publicly accessible archive and a searchable bibliography - by issue, author, title and topic; with a list of editors.

As technological tools of graphic design changed since the journal was established in 1995, and files were lost, part of this archive was made possible with the generous help of the [Multimedia Institute](https://mi2.hr/en/) in Zagreb and the book scanner at the Cultural and Social Centre MaMa. The project of the digital archive of Frakcija was mostly a volunteer undertaking of scanning old issues and their processing, carried out since 2014 by Ivana Ivković, Tomislav Medak, Maja Mihaljević and Renata Šparada.

The Digital Archive of Frakcija Performing Arts Journal is part of the media collection of [The Internet Archive](https://archive.org), a digital library with the stated mission of universal access to all knowledge. All issues can be browsed online or downloaded in several formats, including PDF, Kindle and epub.

The project was supported by [Kultura Nova Foundation](https://kulturanova.hr/english) in 2019 and this finally allowed the completion of the project and the public and permanent release of this archive.

<br><br>
![Kultura nova](/images/Kultura_nova_logo.jpg){ width=25% }
