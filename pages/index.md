<!--
.. title: Dobrodošli u digitalnu arhivu časopisa Frakcija!
.. author: Centar za dramsku umjetnost / Centre for Drama Art
.. date: 2019-12-02 19:52:05 UTC
.. previewimage: /images/Frakcija_01.jpg
.. description: Digitalna arhiva časopisa za izvedbene umjetnosti Frakcija, koji je izlazio od 1995. do 2015., sadrži brojeve od prvog do sedamedestprvog u PDF formatu i detaljnu bibliografiju.
-->

***

# Povijest Frakcije

Projekt javne digitalne arhive časopisa za izvedbene umjetnosti Frakcija koji je izlazio u periodu 1995.-2015. godine omogućuje profesionalnoj i široj javnosti uvid u dva desetljeća jednog od dugovječnijih publicističkih projekata u Hrvatskoj, ne samo u nezavisnoj kulturi, nego i u području kulturnih časopisa općenito.

Izdavač časopisa Frakcija, [Centar za dramsku umjetnost](http://arhiva.cdu.hr/news/index.php), osnovan je 1995. godine uz potporu Instituta Otvoreno društvo, te u suradnji sa [Akademijom dramske umjetnosti](https://adu.hr/) u Zagrebu, pokreće časopis Teatar & Teorija (T&T), produkciju dokumentarnog filma [Factum](http://factum.com.hr/hr/factum) i Imaginarnu akademiju u Grožnjanu. Iste je godine uslijedilo osnivanje tada magazina za izvedbene umjetnosti Frakcija. Tijekom dva desetljeća izdavanja Frakcija se transformira u dvojezičan (hrvatski i engleski), međunarodno relevantan časopis koji okuplja vrhunske suradnike i sudjeluje u mnogim institucionalnim i vaninstitucionalnim projektima.

Započeta iz potrebe artikulacije teorijsko-kritičke platforme za suvremena kretanja u izvedbenim umjetnostima, Frakcija je aktivno gradila kontekst za novu i aktualnu umjetnost te pružila podršku cjelokupnoj nezavisnoj sceni u Hrvatskoj što je, pogotovo u devedesetima, značilo i opozicijsku političku opciju i otpor prema dominantnim kulturnim politikama. Ovaj sadržajno progresivan i međunarodno relevantan časopis nositelj je pamćenja niza lokalnih organizacija, institucija, umjetnika i umjetničkih skupina koje djeluju u Hrvatskoj 1995.-2015.

Od samih početaka Frakcija je uključivala i pregled rada drugih aktera scene - Eurokaz, Tjedan suvremenog plesa, ARL Dubrovnik, Urban festival, Teatar &TD, Teatar EXIT, Montažstroj, Trafik, BADco. i mnogih drugih. Centar za dramsku umjetnost aktivno je sudjelovao u nizu suradničkih platormi i mreža, uključujući Zagreb - Kulturni kapital Evrope 3000, [Savez udruga Klubtura](https://www.clubture.org/), te [Savez udruga Operacija Grad](https://operacijagrad.net/savez-udruga-operacija-grad/). Izdanja časopisa svojom su uredničkom politikom te sadržajem priloga ne samo pratila već i podržavala i suoblikovala kretanja na lokalnoj kulturno-politićkoj sceni, a suradnici časopisa bili su ključni akteri pomaka u polju kulturne politike u dva desetljeća izlaženja, mnogi su to i danas (Vjeran Zuppa, Goran Sergej Pristaš, Ivica Buljan, Milan Živković, Emina Višnić, Marin Blažević i dr.).

U drugom desetljeću svoj postojanja, kao dvojezično izdanje, Frakcija širi krug svojih čitatelja u inozemstvu te time kreira komunikaciju između hrvatskih, europskih i svjetskih teoretičara, teatrologa, umjetnika, stručne i zainteresirane javnosti. Sudeći prema međunarodnim reakcijama, inicijativama samog časopisa, suradnjama s drugim časopisima ([Performance Research](https://www.performance-research.org/), [Maska](http://www.maska.si/index.php?id=161), The Drama Review...), centrima za izvedbene umjetnosti (Chapter Arts Centre Cardiff, Kampnagel Hamburg, Intercult Stockholm, TQW, Tanzkongress), sveučilišnim institucijama (Dartington College of Arts, DasArts Amsterdam, University of Wisconsin-Milwaukee) i strukovnim mrežama (Performance Studies International, Theorem) s kojima je Frakcija iz godine u godinu ulazila u nove su-izdavačke i druge projekte, riječ je uistinu o jedinstvenom pothvatu.

Frakcija je višekratni dobitnik najprestižnijih međunarodnih nagrada za dizajn u rukama dizajnera Igora Masnjaka, CavarPayer, Laboratoriuma i Dejana Dragosavca Rute: European Regional Design Annual Certificate of Design Excellence 1998., Annual Design Review Certificate of Excellence 2003., YoungGuns International Advertising & Design Award 2002., Icograda Excellence Award 2003., iF communication design award 2005. Uz pojavljivanje na nizu domaćih i međunarodnih izložbi, Frakcija samostalno izlaže u galeriji [Hrvatskog dizajnerskog društva](http://dizajn.hr/fragmenti-dizajnerske-povijesti-dokumenti-vol-1/3-drugi-rakurs/3-4-frakcija-1996-2010/) 2010. godine povodom petnaeste godine časopisa (kustos: Maroje Mrduljaš).

# O digitalnoj arhivi

Projekt digitalne arhive Frakcije uključuje digitalizaciju svih izdanja časopisa, trajnu i javno dostupnu arhivu te bibliografiju pretraživu po izdanju, autoru, naslovu i temi, uz popis uredništava.

Kako su se od osnivanja časopisa 1995. godine tehnološki alati dizajnera mijenjali a datoteke gubile, dio arhiva čine izdanja skenirana uz izdašnu pomoć [Multimedijalnog instituta](https://mi2.hr/) i skenera u Kulturnom i društvenom centru MaMa. Projekt digitalnog arhiva časopisa Frakcija većim dijelom je volonterski pothvat skeniranja starih izdanja i obrade materijala na kojem od 2014. rade Ivana Ivković, Tomislav Medak, Maja Mihaljević i Renata Šparada.

Arhiva časopisa za izvedbene umjetnosti Frakcija dio je medijske zbirke [Internetskog arhiva (The Internet Archive)](https://arhive.org), on-line knjižnice i arhiva mrežnih i multimedijalnih sredstava. Svako izdanje moguće je prelistati online ili preuzeti u nizu formata, uključujući PDF, Kindle, epub, i dr.

Projekt je u 2019. godini podržala [Zaklada Kultura nova](https://kulturanova.hr/) i time omogućila dovršetak te javnu i trajnu objavu ove arhive.

<br><br>
![Kultura nova](/images/Kultura_nova_logo.jpg){ width=25% }
