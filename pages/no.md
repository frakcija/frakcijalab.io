<!--
.. title: Digitalna arhiva / Digital archive
.. author: Centar za dramsku umjetnost / Centre for Drama Art
.. date: 2019-12-02 19:52:05 UTC
.. slug: no
.. description: Svi brojevi Frakcije od prvog do sedamdesetprvog. All issues of Frakcija, from the first to the seventy-first.
-->

***

![Frakcija 1](/images/Frakcija_01.jpg){height=250px}
[Frakcija 1](https://archive.org/details/Frakcija_01)
<br>(1995/96)
<br><br>

***

![Frakcija 2](/images/Frakcija_02.jpg){height=250px}
[Frakcija 2](https://archive.org/details/Frakcija_02)
<br>Eurokaz - deset godina
<br>10 years of Eurokaz, the international festival of new theatre
<br>(1996)
<br><br>

***

![Frakcija 3](/images/Frakcija_03.jpg){height=250px}
[Frakcija 3](https://archive.org/details/Frakcija_03)
<br>Alternative; Artaud - 100 godina
<br>Alternatives; Artaud - 100 years
<br>(1997)
<br><br>

***

![Frakcija 4](/images/Frakcija_04.jpg){height=250px}
[Frakcija 4](https://archive.org/details/Frakcija_04)
<br>Tijelo - tehnologija
<br>Body - Technology
<br>(1997)
<br><br>

***

![Frakcija 5](/images/Frakcija_05.jpg){height=250px}
[Frakcija 5](https://archive.org/details/Frakcija_05_)
<br>Što možemo činiti riječima
<br>What can we do with words
<br>(1997)
<br><br>

***

![Frakcija 6/7](/images/Frakcija_06_07.jpg){height=250px}
[Frakcija 6/7](https://archive.org/details/Frakcija_06_07)
<br>Političnost 90ih; Brecht - 100 godina; Ruski akcionizam
<br>Politics of the nineties; Brecht - 100 years; Russian Actionism
<br>(1998)
<br><br>

***

![Frakcija 8](/images/Frakcija_08.jpg){height=250px}
[Frakcija 8](https://archive.org/details/Frakcija_08)
<br>O festivalima; 15 godina Tjedna suvremenog plesa; Brecht - 100 godina II.
<br>On festivals; 15 years of Dance Week Festival; Brecht - 100 years II
<br>(1998)
<br><br>

***

![Frakcija 9](/images/Frakcija_09.jpg){height=250px}
[Frakcija 9](https://archive.org/details/Frakcija_09)
<br>Utopija - distopija
<br>Utopia - Dystopia
<br>(1998)
<br><br>

***

![Frakcija 10/11](/images/Frakcija_10_11.jpg){height=250px}
[Frakcija 10/11](https://archive.org/details/Frakcija_10_11)
<br>Camillo - teatar memorije
<br>Camillo - Theatre of Memory
<br>(1999)
<br><br>

***

![Frakcija 12/13](/images/Frakcija_12_13.jpg){height=250px}
[Frakcija 12/13](https://archive.org/details/Frakcija_12_13)
<br>O(ko) seksualnosti; Judith Butler; Ikonoklazam
<br>On (the eye) of sexuality; Judith Butler; Iconoclasm
<br>(1999)
<br><br>

***

![Frakcija 14](/images/Frakcija_14.jpg){height=250px}
[Frakcija 14](https://archive.org/details/Frakcija_14)
<br>disOrijentacija: Istočna Europa
<br>disOrientation: Eastern Europe
<br>(1999)
<br><br>

***

![Frakcija 15](/images/Frakcija_15.jpg){height=250px}
[Frakcija 15](https://archive.org/details/Frakcija_15)
<br>Uznemirujuće slike - ometanje slike: Chapterova sezona ikonoklastičkog kazališta
<br>Disturbing (the) image: Chapter Season of Iconoclastic Theatre
<br>(1999)
<br><br>

***

![Frakcija 16](/images/Frakcija_16.jpg){height=250px}
[Frakcija 16](https://archive.org/details/Frakcija_16)
<br>90e - argumenti za budućnost
<br>90s - arguments for the future
<br>(2000)
<br><br>

***

![Frakcija 17/18](/images/Frakcija_17_18.jpg){height=250px}
[Frakcija 17/18](https://archive.org/details/Frakcija_17_18)
<br>2000. - Kulturna politika: prva petoljetka; Eugenio Barba
<br>2000 - Cultural politics: first five years; Eugenio Barba
<br>(2000)
<br><br>

***


![FAMA](/images/FAMA.jpg){height=250px}
[FAMA](https://archive.org/details/Frakcija_FAMA)
<br>Tijelo - Razlika
<br>Body - Difference
<br>(2000)
<br><br>

***

![Frakcija 19](/images/Frakcija_19.jpg){height=250px}
[Frakcija 19](https://archive.org/details/Frakcija_19)
<br>Energija
<br>Energy
<br>(2001)
<br><br>

***

![Frakcija 20/21](/images/Frakcija_20_21.jpg){height=250px}
[Frakcija 20/21](https://archive.org/details/Frakcija_20_21)
<br>Glumac i/kao autor
<br>Actor as/and Author
<br>(2001)
<br><br>

***

![Frakcija 22/23](/images/Frakcija_22_23.jpg){height=250px}
[Frakcija 22/23](https://archive.org/details/Frakcija_22_23)
<br>Radikalizmi Istočne Europe
<br>Radicalisms of Eastern Europe
<br>(2001/02)
<br><br>

***

![Frakcija 24/25](/images/Frakcija_24_25.jpg){height=250px}
[Frakcija 24/25](https://archive.org/details/Frakcija_24_25)
<br>Spontanitet – događajnost – improvizacija
<br>Spontaneity – Eventality – Improvisation
<br>(2002)
<br><br>

***

![Frakcija 26/27](/images/Frakcija_26_27.jpg){height=250px}
[Frakcija 26/27](https://archive.org/details/Frakcija_26_27)
<br>(2002/03)
<br><br>

***

![Frakcija 28/29](/images/Frakcija_28_29.jpg){height=250px}
[Frakcija 28/29](https://archive.org/details/Frakcija_28_29)
<br>Nejasno nepostojano nerazumljivo
<br>Vague Volatile Incomprehensible
<br>(2003)
<br><br>

***

![Frakcija 30/31](/images/Frakcija_30_31.jpg){height=250px}
[Frakcija 30/31](https://archive.org/details/Frakcija_30_31)
<br>Proizvodnja zajedničkog
<br>Production of the Common
<br>(2003)
<br><br>

***

![Frakcija 32/35](/images/Frakcija_32_35.jpg){height=250px}
[Frakcija 32/35](https://archive.org/details/Frakcija_32_35)
<br>Goat Island "When will the september..." PRVI DIO Refleksije procesa
<br>Goat Island's "When will the september roses bloom? Last night was only a comedy" PART ONE, Reflections on the Process
<br>(2004)
<br><br>

***

![Frakcija 33/34](/images/Frakcija_33_34.jpg){height=250px}
[Frakcija 33/34](https://archive.org/details/Frakcija_33_34)
<br>Save as city.doc
<br>(2004)
<br><br>

***

![Frakcija 36](/images/Frakcija_36.jpg){height=250px}
[Frakcija 36](https://archive.org/details/Frakcija_36)
<br>Grupne Dinamike
<br>Group Dynamics
<br>(2005)
<br><br>

***

![Frakcija - Maska - Performance Research](/images/Performance_research_10_02.jpg){height=250px}
[Frakcija - Maska - Performance Research](http://www.performance-research.org/past-issue-detail.php?issue_id=32)
<br>On Form - Yet to Come
<br>(2005)
<br><br>

***

![Frakcija 37/38](/images/Frakcija_37_38.jpg){height=250px}
[Frakcija 37/38](https://archive.org/details/Frakcija_37_38)
<br>Retorika
<br>Rhetoric
<br>(2005/6)
<br><br>

***

![Frakcija 39/40](/images/Frakcija_39_40.jpg){height=250px}
[Frakcija 39/40](https://archive.org/details/Frakcija_39_40)
<br>Rad, virtuoznost i institucije
<br>On Labor, Virtuosity and Institutions
<br>(2006)
<br><br>

***

![Frakcija 41](/images/Frakcija_41.jpg){height=250px}
[Frakcija 41](https://archive.org/details/Frakcija_41)
<br>Autoceste znanja
<br>Highways of Knowledge
<br>(2006)
<br><br>

***

![Frakcija 42](/images/Frakcija_42.jpg){height=250px}
[Frakcija 42](https://archive.org/details/Frakcija_42)
<br>Političke rekonstrukcije života
<br>Political Reconstructions of Life
<br>(2007)
<br><br>

***

![Frakcija 43/44](/images/Frakcija_43_44.jpg){height=250px}
[Frakcija 43/44](https://archive.org/details/Frakcija_43_44)
<br>Stalno u oku kamere
<br>Constant Capture
<br>(2007)
<br><br>

***

![Frakcija 45/46](/images/Frakcija_45_46.jpg){height=250px}
[Frakcija 45/46](https://archive.org/details/Frakcija_45_46)
<br>Tranzistor
<br>(2007/08)
<br><br>

***

![Frakcija 47/48](/images/Frakcija_47_48.jpg){height=250px}
[Frakcija 47/48](https://archive.org/details/Frakcija_47_48)
<br>Koreografija, tangencionalno
<br>Circumscribing Choreography
<br>(2008)
<br><br>

***

![Frakcija 49](/images/Frakcija_49.jpg){height=250px}
[Frakcija 49](https://archive.org/details/Frakcija_49)
<br>(2008)
<br><br>

***

![Frakcija 50](/images/Frakcija_50.jpg){height=250px}
[Frakcija 50](https://archive.org/details/Frakcija_50)
<br>PSi#15SHIFTS
<br>(2009)
<br><br>

***

![Frakcija 51/52](/images/Frakcija_51_52.jpg){height=250px}
[Frakcija 51/52](https://archive.org/details/Frakcija_51_52)
<br>Što afirmirati? Što izvoditi?
<br>What to Affirm? What to Perform?
<br>(2009)
<br><br>

***

![Frakcija 53/54](/images/Frakcija_53_54.jpg){height=250px}
[Frakcija 53/54](https://archive.org/details/Frakcija_53_54)
<br>Process:Music
<br>(2009/10)
<br><br>

***

![Frakcija 55](/images/Frakcija_55.jpg){height=250px}
[Frakcija 55](https://archive.org/details/Frakcija_55)
<br>Curating Performing Arts
<br>(2010)
<br><br>

***

![Frakcija 56/57](/images/Frakcija_56_57.jpg){height=250px}
[Frakcija 56/57](https://archive.org/details/Frakcija_56_57)
<br>Dramsko pismo 00
<br>Playwriting 00
<br>(2010)
<br><br>

***

![Frakcija 58/59](/images/Frakcija_58_59.jpg){height=250px}
[Frakcija 58/59](https://archive.org/details/Frakcija_58_59)
<br>to be moved
<br>(2011)
<br><br>

***

![Frakcija 60/61](/images/Frakcija_60_61.jpg){height=250px}
[Frakcija 60/61](https://archive.org/details/Frakcija_60_61)
<br>Umjetnički rad u doba štednje
<br>Artistic Labor in the Age of Austerity
<br>(2011)
<br><br>

***

![Frakcija 62/63](/images/Frakcija_62_63.jpg){height=250px}
[Frakcija 62/63](https://archive.org/details/Frakcija_62_63)
<br>Actionable Image: Agency of Image, Performance of Body, Apparatus of Spectating
<br>(2012)
<br><br>

***

![Frakcija 64/65](/images/Frakcija_64_65.jpg){height=250px}
[Frakcija 64/65](https://archive.org/details/Frakcija_64_65)
<br>The Art of the Concept
<br>(2012)
<br><br>

***

![Frakcija 66/67](/images/Frakcija_66_67.jpg){height=250px}
[Frakcija 66/67](https://archive.org/details/Frakcija_66_67)
<br>Imunitet umjetnosti
<br>The Immunity of Art
<br>(2013)
<br><br>

***

![Frakcija 68/69](/images/Frakcija_68_69.jpg){height=250px}
[Frakcija 68/69](https://archive.org/details/Frakcija_68_69)
<br>Umjetnost i novac
<br>Art and Money
<br>(2013)
<br><br>

***

![Frakcija 70/71](/images/Frakcija_70_71.jpg){height=250px}
[Frakcija 70/71](https://archive.org/details/Frakcija_70_71)
<br>Znanje drugim sredstvima
<br>Knowledge by other means
<br>(2014/15)
<br><br>
